class Human{
    constructor(name, address){
    this.name = name;  this.address = address;
  }

  // public instance method
  introduce(){
  console.log(`Hello, my name is ${this.name}`)
  }

  //public static method
  static isEating(food) {
    let foods = ["plant", "animal"];
    return foods.includes(food.toLowerCase());
  }
}

let ar = new Human("Arif Rusman", "Riau");  console.log(ar)
console.log(ar.introduce());
console.log(Human.isEating("Plant"))
console.log(Human.isEating("Human"))
  