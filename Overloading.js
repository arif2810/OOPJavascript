class Person{
  constructor(name, address){
    this.name = name;
    this.address = address;
  }

  introduce(){
    console.log(`Hi, my name is ${this.name}`);
  }
}

class Programmer extends Person{
  constructor(name, address, programmingLanguage){
    super(name, address);
    this.programmingLanguage = programmingLanguage;
  }

  // Overload method introduce
  introduce(withDetail){
    super.introduce();
    (Array.isArray(withDetail));
    console.log(`I can write`, this.programmingLanguage);
    console.log("Wrong input");
  }

  code(){
    let acak = Math.floor(Math.random() * this.programmingLanguage.length)
    console.log(
      "Code some",
      this.programmingLanguage[acak],
    )
  }
}


let arif = new Programmer("Arif Rusman", "Riau", ["PHP", "Javascript", "Python"]);
arif.introduce(["Javascript"]);
arif.code();


