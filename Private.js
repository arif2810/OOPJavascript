class Human{
  constructor(name, address){
    this.name = name;
    this.address = address;
  }

  #doGossip = () => {
    console.log(`My address will become viral ${this.address}`)
  }

  talk(){
    console.log(this.#doGossip()); // Call the private method
  }

  static #isHidingArea = true;
}


let ar = new Human("Arif Rusman", "Riau");
console.log(ar.talk())

// try{
//   Human.#isHidingArea // Tidak bisa mengakses property ##isHidingArea
//   ar.#doGossip() // Tidak mengakses method #doGossip()
// }
// catch(err){
//   console.error(err)
// }



