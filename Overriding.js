class Person{
  constructor(name, address){
    this.name = name;
    this.address = address;
  }

  introduce(){
    console.log(`Hi, my name is ${this.name}`);
  }
}

class Programmer extends Person{
  constructor(name, address, programmingLanguage){
    super(name, address);
    this.programmingLanguage = programmingLanguage;
  }

  // Override method introduce
  introduce(){
    super.introduce();
    console.log(`I can write`, this.programmingLanguage);
  }

  code(){
    console.log(
      "Code some",
      this.programmingLanguage[
        Math.floor(Math.random() * this.programmingLanguage.length)
      ]
    )
  }
}

let arif = new Programmer("Arif Rusman", "Riau", ["PHP", "Javascript", "Python"]);
arif.introduce();


