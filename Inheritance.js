class Human{
  // Constructor Method
  constructor(name, address){
    this.name = name;
    this.address = address;
  }

  introduce(){
    console.log(`Hi, my name is ${this.name}`);
  }

  work(){
    console.log("Work");
  }
}

class Programmer extends Human{
  constructor(name, address, programmingLanguage){
    super(name, address);
    this.programmingLanguage = programmingLanguage;
  }

  introduce(){
    super.introduce();
    console.log(`I can write `, this.programmingLanguage);
  }

  code(){
    console.log(
      "Code some",
      this.programmingLanguage[
        Math.floor(Math.random() * this.programmingLanguage.length)
      ]
    )
  }
}


// Inisialisasi langsung dari class Human
let arif = new Human("Arif Rusman", "Riau");
arif.introduce();

let fitria = new Programmer("Fitria", "Jakarta", ["PHP", "Javascript", "Python"]);
fitria.introduce();
fitria.code();
fitria.work();

// try{
//   arif.code();
// }

// catch(error){
//   console.log(err.message);
// }

console.log(fitria instanceof Human);
console.log(fitria instanceof Programmer);
