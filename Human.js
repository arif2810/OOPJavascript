class Human{
  // Static Property
  static isLivingOnEarth = true;

  // Constructor Method
  constructor(name, address){
    this.name = name;
    this.address = address;
  }

  // Instance Method Signature
  introduce(){
    console.log(`Hi, my name is ${this.name}`);
  }
}

console.log(Human.isLivingOnEarth);

// ==================================================

// Menambahkan prototype / instance Method
Human.prototype.greet = function(name){
  console.log(`Hi, ${name}, I'm ${this.name}`);
}

// Menambahkan static method
Human.destroy = function(thing){
  console.log(`Human is destroying ${thing}`);
}

// Instansiasi class Human
let ar = new Human("Arif Rusman", "Riau");
console.log(ar);

// Mengecek instance dari class
console.log(ar instanceof Human);

console.log(ar.introduce());
console.log(ar.greet("Fitria Damayanti"));
console.log(Human.destroy("Love"));
